import { Dimensions } from 'react-native';
export const { width, height } = Dimensions.get('window');

import persianDate from './functions/PersianDate'
import persianDateTime from './functions/PersianDateTime'
import normalize from './functions/Normalize'
import S2N from './functions/Strint2Number'
import checkZero from './functions/CheckZero'
import validateEmail from './functions/EmailValidation'
import checkEmpty from './functions/CheckEmpty'
import checkOtp from './functions/CheckOtp'
import passValidation from './functions/PassValidation'
import separate from './functions/Separate'
import vSeparator from './functions/VSeparator'
import banks from './functions/BanksName'
import uuidv4 from './functions/uuidv4'



export { persianDate, persianDateTime, normalize, S2N, checkZero, validateEmail, checkEmpty, checkOtp, passValidation, separate, vSeparator, banks, uuidv4 }