
export default function vSeparator(
    target,
    repeatCount = 4,
    separator = '-',
    type = 'd',
    regString = '\\d{1,4}',
) {
    //make filter regex
    let replaceString = '[^\\d]';
    replaceString = replaceString.replace(/d/gi, type);
    const regex1 = new RegExp(replaceString, 'g');

    //filter target
    target = target + '';
    target = target.replace(regex1, '');

    if (target) {
        //make separator regex
        regString = regString.replace(/d/gi, type);
        regString = regString.replace(/4/g, repeatCount);
        const regex = new RegExp(regString, 'g');

        //separate
        return target.match(regex).join(separator);
    }
}