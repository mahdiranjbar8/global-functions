import PropTypes from 'prop-types';
import toast from "./toasty";

export default function ValidateEmail(
    email = PropTypes.string.isRequired,
) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
        console.log('Email is Not Correct');

        toast('ایمیل وارد شده صحیح نیست', 'Show');
        return false;
    } else {
        console.log('Email is Correct');
        return true;
    }
}