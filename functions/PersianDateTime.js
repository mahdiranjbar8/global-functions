import PropTypes from 'prop-types';
import moment from 'moment-jalaali';

export default function persianDateTime(
    params = PropTypes.string.isRequired
) {
    return moment(params, 'YYYY-MM-DDTHH:mm:ss').format('jYYYY/jMM/jDD - HH:mm');
}