import PropTypes from 'prop-types';
import toast from "./toasty";

export default function validPass(
    password = PropTypes.string.isRequired,
    type,
    message = 'رمز باید حداقل 8 کاراکتر باشد و شامل حرف بزرگ ، حرف کوچک و عدد باشد',
    reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/
) {
    // let re = ;
    if (reg.test(password) === false) {
        console.log('password is Not Correct');
        type
            ? null
            : toast(
                message,
                'Show',
            );
        return false;
    } else {
        return true;
    }
}