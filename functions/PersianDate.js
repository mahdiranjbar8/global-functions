import PropTypes from 'prop-types';
import moment from 'moment-jalaali';

export default function persianDate(
    params = PropTypes.string.isRequired
) {
    return moment(params, 'YYYY-M-D HH:mm:ss')
        .endOf('jMonth')
        .format('jYYYY/jMM/jDD');
}