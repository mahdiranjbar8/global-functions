import { RNToasty } from 'react-native-toasty';

export default function toast(message, type, position = 'bottom') {
    switch (type) {
        case 'Error':
            RNToasty.Error({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
        case 'Info':
            RNToasty.Info({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
        case 'Success':
            RNToasty.Success({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
        case 'Warn':
            RNToasty.Warn({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
        case 'Show':
            RNToasty.Show({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
        default:
            RNToasty.Normal({
                title: message,
                fontFamily: 'Vista',
                position: position,
                titleSize: 14,
                withIcon: false,
            });
            break;
    }
};