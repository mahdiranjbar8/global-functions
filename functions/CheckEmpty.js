import PropTypes from 'prop-types';
import toast from "./toasty";

export function checkEmpty(
    text = PropTypes.string.isRequired,
    name = PropTypes.string.isRequired
) {
    if (text != null) {
        if (text.length != 0) {
            return true;
        } else {
            toast(`${name} را وارد کنید`, 'Show');
            return false;
        }
    } else {
        toast(`${name} را وارد کنید`, 'Show');
        return false;
    }
}