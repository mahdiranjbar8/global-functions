import PropTypes from 'prop-types';
import toast from "./toasty";

export default function checkOtpCode(
    value = checkPropTypes.string.isRequired,
    length = PropTypes.number.isRequired
) {
    if (value.length === length) {
        return true;
    } else {
        toast('کد را وارد کنید', 'Show');
        return false;
    }
}