import PropTypes from 'prop-types';
import S2N from "./Strint2Number";

function checkZero(
    value = PropTypes.string || PropTypes.number,
) {
    return S2N(value) === 0;
};

export default checkZero;